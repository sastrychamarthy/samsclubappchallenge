//
//  ProductDetailTests.swift
//  SamsClubAppChallengeTests
//
//  Created by Sastry Chamarthy on 2/6/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import XCTest
@testable import SamsClubAppChallenge

class ProductDetailTests: XCTestCase {
    
    func testProductDetails() {
        do {
            let response: ProductDetail = try TestHelper.decodedObject(fromFileNamed: "ProductDetailResponse")
            let productId = response.productId
            let productName = response.productName
            let productImage = response.productImage
            let price = response.price
            let rating = response.reviewRating
            let inStock = response.inStock
            let shortDesc = response.shortDescription
            let longDesc = response.longDescription
            XCTAssert(productId == "003e3e6a")
            XCTAssert(productName == "Ellerton TV Console")
            XCTAssert(productImage == "/images/image2.jpeg")
            XCTAssert(price == "$949.00")
            XCTAssert(rating == 2.0)
            XCTAssert(inStock == true)
            XCTAssert(shortDesc == "<p><span style=\"color:#FF0000;\"><b>White Glove Delivery Included</b></span></p>\n\n<ul>\n\t<li>Excellent for the gamer, movie enthusiest, or interior decorator in your home</li>\n\t<li>Built-in power strip for electronics</li>\n\t<li>Hardwood solids and cherry veneers</li>\n</ul>\n")
            XCTAssert(longDesc == "<p>The Ellerton media console is well-suited for today&rsquo;s casual lifestyle. Its elegant style and expert construction will make it a centerpiece in any home. Soundly constructed, the Ellerton uses hardwood solids &amp; cherry veneers elegantly finished in a rich dark cherry finish. &nbsp;With ample storage for electronics &amp; media, it also cleverly allows for customization with three choices of interchangeable door panels.</p>\n")
            
        } catch {
            XCTFail()
        }
    }
    
    func testProductDetailsWithNoProductDescription() {
        do {
            let response: ProductDetail = try TestHelper.decodedObject(fromFileNamed: "ProductResponseSanitized")
            let productId = response.productId
            let productName = response.productName
            let productImage = response.productImage
            let price = response.price
            let rating = response.reviewRating
            let inStock = response.inStock
            let shortDesc = response.shortDescription
            let longDesc = response.longDescription
            XCTAssert(productId == "003e3e6a")
            XCTAssert(productName == "Ellerton TV Console")
            XCTAssert(productImage == "/images/image2.jpeg")
            XCTAssert(price == "$949.00")
            XCTAssert(rating == 2.0)
            XCTAssert(inStock == true)
            XCTAssert(shortDesc == nil)
            XCTAssert(longDesc == nil)
            
        } catch {
            XCTFail()
        }
    }
}
