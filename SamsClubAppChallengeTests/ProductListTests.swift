//
//  ProductListTests.swift
//  SamsClubAppChallengeTests
//
//  Created by Sastry Chamarthy on 2/6/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import XCTest
@testable import SamsClubAppChallenge

class ProductListTests: XCTestCase {
    
    func testProductList() {
        do {
            let response: ProductList = try TestHelper.decodedObject(fromFileNamed: "Products")
            let products = response.products
            let statusCode = response.statusCode
            let totalProducts = response.totalProducts
            let pageSize = response.pageSize
            let pageNumber = response.pageNumber
            XCTAssert(products.count == 1)
            XCTAssert(statusCode == 200)
            XCTAssert(totalProducts == 224)
            XCTAssert(pageNumber == 10)
            XCTAssert(pageSize == 15)
        } catch {
            XCTFail()
        }
    }
}
