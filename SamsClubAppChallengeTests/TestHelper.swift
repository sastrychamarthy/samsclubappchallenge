//
//  TestHelper.swift
//  SamsClubAppChallengeTests
//
//  Created by Sastry Chamarthy on 2/6/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation

class TestHelper: NSObject {
    
    static func testJSONData(withFileName fileName: String) -> Data {
        return try! Data(contentsOf: Bundle(for: self.classForCoder()).url(forResource: fileName, withExtension: "json")!)
    }
    
    static func decodedObject<T: Decodable>(fromFileNamed fileName: String, with decoder: JSONDecoder = JSONDecoder()) throws -> T {
        let data = testJSONData(withFileName: fileName)
        return try decoder.decode(T.self, from: data)
    }
}

