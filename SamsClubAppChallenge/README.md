#  SamsClubAppChallenge 

Description:
This app contains 2 screens 
1. Product List Screen
2. Product Details Screen

The codebase is organized into the following groups
1. API
2. ViewController
3. View
4. ViewModel
5. Model
6. Animator
7. Utilities
8. SamsClubAppChallengeTests

Project Description:
1. Networking

The products are fetched from the endpoint using the NSURLSession and the corresponding logic can be found in the ProductListAPI Class. 

2. Data Handling/Transforming
Swift 4 provides Codable protocol to transform JSON objects into models and the same has been used to transform the products and the product list object to be consumed by the UI.

3. Utility Functions
a) I have created a LazyTableView Class to support the lazy load of products as the user scrolls
b) I have also created extensions around some commonly used code like loading of viewControllers from storyboards, loading of tableViewCells from nibs, imageView to support download of images from web and and Alert Controllers to show alerts when needed

4. Product Details uses the MVVM approach so that the view model can be easily unit-tested
5. This app supports iPhone in Portrait and iPad in landscape, I have used size classes and setup all the constraints in the storyboard to support the same.
6. Product List tableview supports self sizing and the self sizing tableview cells have been utilized to achieve the same. There is a label at the bottom of the screens which shows the count of products loaded and gets updated as more products are fetched and loaded onto the screen.
7. I prefer to use XIBs over prototype cells in the storyboard as the cells designed in the storyboard cannot be reused. This has been done for future extensibility. 

8. Added unit tests around data transformation and the ProductDetailViewModel.

9. Added Custom Push and Pop Animations while presenting the product detail
