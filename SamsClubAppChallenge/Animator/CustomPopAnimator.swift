//
//  CustomPopAnimator.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation

import UIKit

class CustomPopAnimator: CustomAnimator {
    
    let interactionController: UIPercentDrivenInteractiveTransition?
    
    public init(type: TransitionType,
                duration: TimeInterval = 0.25,
                interactionController: UIPercentDrivenInteractiveTransition? = nil) {
        self.interactionController = interactionController
        
        super.init(type: type, duration: duration)
    }
    
    override func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromViewController = transitionContext.viewController(forKey: .from)
            else {
                return
        }
        
        if self.type == .navigation, let toViewController = transitionContext.viewController(forKey: .to) {
            transitionContext.containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)
        }
        
        let duration = self.transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, animations: {
            fromViewController.view.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
            fromViewController.view.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
