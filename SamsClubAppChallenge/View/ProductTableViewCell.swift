//
//  ProductTableViewCell.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet var productName: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var availabilityLabel: UILabel!
    @IBOutlet var productImageView: UIImageView!
    
    func configureWithProduct(productDetailViewModel: ProductDetailViewModel) {
        productName.text = productDetailViewModel.productName
        priceLabel.text = productDetailViewModel.productPrice
        ratingLabel.text =  productDetailViewModel.productRating
        availabilityLabel.text = productDetailViewModel.productAvailabilityString
        imageView?.image = UIImage(named: "placeholder")
        imageView?.downloadImageFrom(link: productDetailViewModel.productImageString, contentMode: .scaleAspectFill)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
        productName.text = nil
        priceLabel.text = nil
        availabilityLabel.text = nil
        ratingLabel.text = nil
    }
    
}
