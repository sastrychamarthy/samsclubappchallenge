//
//  ProductList.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation

struct ProductList: Codable {
    
    let products: [ProductDetail]
    let totalProducts: Int
    let pageNumber: Int
    let pageSize: Int
    let statusCode: Int
}
