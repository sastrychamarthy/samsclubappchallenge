//
//  ProductDetailViewController.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//
import Foundation
import UIKit

class ProductDetailViewController: UIViewController {
    
    @IBOutlet var productName: UILabel!
    @IBOutlet var productDescription: UILabel!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productRating: UILabel!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var productLongDescription: UILabel!
    @IBOutlet var productAvailabilityLabel: UILabel!
    @IBOutlet var productIDLabel: UILabel!
    @IBOutlet var productDescHeaderLabel: UILabel!
    
    var productViewModel: ProductDetailViewModel?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        guard let productViewModel = productViewModel else { return }
        setupUIWithViewModel(viewModel: productViewModel)
    }
    
    func configureWithViewModel(viewModel: ProductDetailViewModel) {
        self.productViewModel = viewModel
    }
    
    private func setupUIWithViewModel(viewModel: ProductDetailViewModel) {
        productName.text = viewModel.productName
        productDescription.attributedText = viewModel.productDescription
        productRating.text = viewModel.productRatingLongDescription
        productPrice.text = viewModel.productPrice
        var contentMode: UIView.ContentMode = .scaleAspectFill
        if traitCollection.horizontalSizeClass == .regular {
            contentMode = .center
        }
        productImageView.downloadImageFrom(link: viewModel.productImageString, contentMode: contentMode)
        productDescHeaderLabel.isHidden = viewModel.productLongDescription?.length == 0
        productLongDescription.attributedText = viewModel.productLongDescription
        productAvailabilityLabel.text = viewModel.productAvailabilityString
        productIDLabel.text = viewModel.productId
    }
    
}
