//
//  LazyLoadTableViewUtility.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation
class LazyLoadTableViewUtility {
    
    var currentCursor: Int = -1
    var enabled: Bool = true
    var pageSize: Int = -1
    
    init() {}
    
    init(withPageSize pSize: Int, andEnabled isEnabled: Bool) {
        self.pageSize = pSize
        self.enabled = isEnabled
    }
    
    func nextCursor() -> Int {
        
        if !self.enabled { return -1 }
        switch currentCursor {
        case -1:
            currentCursor = 0
            break
        case 0:
            currentCursor = self.pageSize
            break
        default:
            currentCursor += pageSize
        }
        return currentCursor
    }
    
    func resetCursor() {
        currentCursor = -1
    }
    
}
