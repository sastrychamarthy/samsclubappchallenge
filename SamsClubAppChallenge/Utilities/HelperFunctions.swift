//
//  HelperFunctions.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    enum StoryboardName: String {
        case Main
    }
    
    convenience init(name: StoryboardName) {
        self.init(name: name.rawValue, bundle: nil)
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        let storyboardIdentifier = String(describing: T.self)
        guard let viewController = self.instantiateViewController(withIdentifier: storyboardIdentifier) as? T else {
            fatalError("Could not load the viewController")
        }
        
        return viewController
    }
}

public extension UITableView {
    
    public func register<T: UITableViewCell>(nib: UINib, forClass cellClass: T.Type) {
        register(nib, forCellReuseIdentifier: cellClass.defaultIdentifier())
    }
    
    public func registerDefaultNibbable<T: UITableViewCell>(cellClass: T.Type) {
        register(nib: cellClass.defaultNib(), forClass: cellClass)
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(withClass cellClass: T.Type, reuseIdentifier: String, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? T else {
            fatalError("Error: cell with identifier: \(reuseIdentifier) for index path: \(indexPath) is not \(T.self)")
        }
        return cell
    }
}

public extension UITableViewCell {
    
    public class func defaultIdentifier() -> String {
        return String(describing: self)
    }
    
    public class func defaultNib() -> UINib {
        return UINib(nibName: defaultIdentifier(), bundle: nil)
    }
}

extension UIImageView {
    
    func downloadImageFrom(link:String, contentMode: UIView.ContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}

extension UIAlertController {
    
    static func Alert(_ title: String, message: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        return alertController
    }
    
    @objc @discardableResult func withOKAction(style: UIAlertAction.Style = .cancel, handler:@escaping (UIAlertAction) -> Void) -> UIAlertController {
        let OKAction = UIAlertAction(title: "OK", style: style) { (action) in  handler(action) }
        self.addAction(OKAction)
        return self
    }
    
    @objc func showFrom(_ viewController: UIViewController) {
        // An exception will be thrown if a viewcontroller tries to present itself
        guard self !== viewController else { return }
        viewController.present(self, animated: true, completion: nil)
    }
}

