//
//  LazyTableView.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

@objc protocol LazyTableViewDelegate
{
    @objc optional func tableView(tableView: UITableView, lazyLoadNextCursor cursor: Int)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
}

class LazyTableView: UITableView, UITableViewDelegate {
    
    var lazyLoadEnabled: Bool!
    var lazyLoadPageSize: Int!
    var currentCursor: Int!
    var lazyLoad: LazyLoadTableViewUtility = LazyLoadTableViewUtility()
    override var delegate: UITableViewDelegate?
        {
        didSet
        {
            super.delegate = self
        }
        
        willSet
        {
            self.senderDelegate = newValue as? LazyTableViewDelegate
        }
    }
    private var senderDelegate: LazyTableViewDelegate?
    
    // MARK: Initialization
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        super.delegate = self
    }
    
    override init(frame: CGRect, style: UITableView.Style)
    {
        super.init(frame: frame, style: style)
        super.delegate = self
    }
    
    // MARK: UITableView UIScrollView Override
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let endScrolling: CGFloat = scrollView.contentOffset.y + scrollView.frame.size.height
        
        if (endScrolling >= scrollView.contentSize.height)
        {
            self.senderDelegate?.tableView?(tableView: self, lazyLoadNextCursor: self.lazyLoad.nextCursor())
            currentCursor = self.lazyLoad.currentCursor
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.senderDelegate?.tableView(tableView, didSelectRowAt: indexPath)
    }
}

