//
//  ProductDetailViewModel.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation
import UIKit

class ProductDetailViewModel {
    
    let productDetail: ProductDetail
    
    init(with productDetail: ProductDetail) {
        self.productDetail = productDetail
    }
    
    var productName: String {
        return productDetail.productName
    }
    
    var productDescription: NSAttributedString? {
        guard let font = font else { return nil }
        return attributedStringForString(productDetail.shortDescription?.html2Attributed, font: font)
    }
    
    var productLongDescription: NSAttributedString? {
        guard let font = font else { return nil }
        return attributedStringForString(productDetail.longDescription?.html2Attributed, font: font)
    }
    
    var productPrice: String {
        return productDetail.price
    }
    
    var productRating: String {
        let ratingString = String(productDetail.reviewRating)
        return "Rating: " + ratingString + "/5"
    }
    
    var productRatingLongDescription: String {
        let reviewCount = "(\(String(productDetail.reviewCount)) Customer Reviews)"
        return productRating + reviewCount
    }
    
    var productAvailabilityString: String {
        return productDetail.inStock ? "Ready to ship" : "Product currently out of stock"
    }
    
    var productImageString: String {
        return APIConstants.baseURL + productDetail.productImage
    }
    
    var productId: String {
        return productDetail.productId
    }
    
}

extension ProductDetailViewModel {
    
    func attributedStringForString(_ string: NSAttributedString?, font: UIFont, color: UIColor = UIColor.black) -> NSMutableAttributedString {
        let stringAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color]
        let result = NSMutableAttributedString()
        if let string = string {
            result.setAttributedString(string)
        }
        result.setAttributes(stringAttributes, range: NSRange(location: 0, length: result.length))
        return result
    }
    
    var font: UIFont? {
        let font = UIFont(name: "Arial", size: 17.0)
        return font
    }
}

