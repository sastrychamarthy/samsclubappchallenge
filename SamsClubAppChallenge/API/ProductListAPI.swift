//
//  ProductListAPI.swift
//  SamsClubAppChallenge
//
//  Created by Sastry Chamarthy on 2/5/19.
//  Copyright © 2019 Sastry Chamarthy. All rights reserved.
//

import Foundation

struct APIConstants {
    static let baseURL = "https://mobile-tha-server.firebaseapp.com"
}

class ProductListAPI {
    
    func getProductList(pageNumber: Int, pageSize: Int, success: @escaping (_ products: ProductList) -> Void, failure: @escaping (_ error: Error?) -> Void) {
        let pageNumberString = String(pageNumber)
        let pageSizeString = String(pageSize)
        let getURL = URL(string: "\(APIConstants.baseURL)/walmartproducts/\(pageNumberString)/\(pageSizeString)")
        guard let url = getURL else { fatalError("Invalid URL") }
        var getRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 30.0)
        getRequest.httpMethod = "GET"
        URLSession.shared.dataTask(with: getRequest, completionHandler: { (data, _, error) -> Void in
            if let responseData = data {
                do {
                    let productList: ProductList = try JSONDecoder().decode(ProductList.self, from: responseData)
                    success(productList)
                } catch {
                    failure(error)
                }
            } else {
                failure(error)
            }
        }).resume()
        
    }
}
